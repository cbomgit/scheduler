Christian Boman
Operating Systems Lab 2
CPU Scheduler Simulations


This program runs a simulation of FCFS, SRTF, and RR cpu schedulers. The main
program takes an input file of processes and runs it through the chosen CPU
scheduler. The scheduler will output statistics about each process to stdout.

FCFS scheduler

To simulate the FCFS scheduler, run the following command:

$PATH_TO_EXECUTABLE/scheduler $PATH_TO_INPUT_FILE 0

The FCFS scheduler uses the queue data structure to keep track of its processes. Queue
functions are implemented in queue.c and defined in queue.h. 


SRTF scheduler

To simulate the SRTF scheduler, run the following command: 

$PATH_TO_EXECUTABLE/scheduler $PATH_TO_INPUT_FILE 1

The SRTF scheduler uses the binary heap or priority queue data structure to keep track
of processes. The process with the shortest time left will always be at the top of the queue.


RR scheduler

$PATH_TO_EXECUTABLE/scheduler $PATH_TO_INPUT_FILE 2 2

Where the last argument is the timeslice that the scheduler will allocate to each process. The
RR scheduler uses the queue functions defined in queue.h.


To build the program run "make" and to clean the program, run "make clean".
