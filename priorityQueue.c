#include"priorityQueue.h"


int addToShortestJobQueue(process p, int *currentSize, process *queue) {

   if(*currentSize == MAX_QUEUE_LENGTH - 1) {
      return 0;
   }
   
   int hole = ++(*currentSize);
   for( ; hole > 1 && p.timeLeft < queue[hole / 2].timeLeft; hole /= 2) {
      queue[hole] = queue[hole / 2];
   }
   queue[hole] = p;
   return 1;
}

process viewShortestJob(int currentSize, process *queue) {

   process ret = { -1, -1, -1 };
   
   if(currentSize > 0)
      ret = queue[1];
      
   return ret;
   
}

process getShortestJob(int *currentSize, process *queue) {

   process ret = { -1, -1 , -1 };
   
   if(currentSize > 0) {
      ret = viewShortestJob(*currentSize, queue);
      queue[1] = queue[(*currentSize)--];
      percolateDown(*currentSize, queue, 1 );
   }
   
   return ret;

}

/*Non public interfaces */
void buildQueue(process *queue, int *currentSize) {
   int i;
   for(i = (*currentSize) / 2; i > 0; i--) {
      percolateDown(*currentSize, queue, i);
   }
}

void percolateDown(int currentSize, process *queue, int hole) {
   int child;
   process tmp = queue[hole];
   for(; hole * 2 <= currentSize; hole = child) {
      child = hole * 2;
      if(child != currentSize && queue[child + 1].timeLeft < queue[child].timeLeft)
         child++;
      if(queue[child].timeLeft < tmp.timeLeft)
         queue[hole] = queue[child];
      else
         break;
   }
   queue[hole] = tmp;
}
