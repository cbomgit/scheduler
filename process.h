#ifndef _PROCESS
#define _PROCESS

#define MAX_QUEUE_LENGTH 100

typedef struct Process {

   int pid;                //the processes identifying ID
   int arrivalTime;        //time the process was submitted to the readyQueue
   int cpuBurst;           //run time of the process
   int lastSwitchTime;     //the last time the process was preempted
   int finishTime;         //the time at which the process finished
   int totalWaitTime;      //the total time this process spent waiting
   int responseTime;       //number of time it took for process to produce output
   int numContextSwitches; //number of times this process was pre-empted
   int timeLeft;           //number of milliseconds left for this process to run
   int lastPreemptTime;    //the time at which this process was preempted
} process;


#endif
