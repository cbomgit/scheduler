#include"queue.h"
#include<stdlib.h>
#include<stdio.h>

void queueProcess(process *readyQueue, int *tail, process newProc, int *size) {

   //fail if we've allocated the maximum amount of processes
   if(*size == MAX_QUEUE_LENGTH) {
      fprintf(stderr, "Could not add process to readyQueue. Aboring.\n");
      exit(EXIT_FAILURE);
   }
   *tail = increment(*tail);
   readyQueue[(*tail)] = newProc;
   (*size)++;
}

process dequeueProcess(process *readyQueue, int *head, int *size) {
   
   //fail if the current Queue is empty
   if(*size == 0) {
      process p = {-1,-1,-1, 0, 0, 0, 0};
      return p;
   }
   (*size)--;
   process p = readyQueue[*head];
   *head = increment(*head);
   return p;
}

int increment(int ndx) {

   if(++ndx == MAX_QUEUE_LENGTH) {
      ndx = 0;
   }
   return ndx;
}
