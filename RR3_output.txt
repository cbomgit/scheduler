Running Round Robin scheduler...
pid       	arrival   	CPU-burst 	finish    	waiting time	turn around	response time	No. of Context
1         	0         	1         	2         	0         	2         	0         	1         
2         	2         	7         	35        	10        	33        	4         	5         
3         	3         	6         	27        	9         	24        	0         	2         
4         	5         	5         	32        	6         	27        	4         	2         
5         	6         	2         	15        	6         	9         	6         	0         
Average CPU Burst Time = 4.20	Average Waiting time = 6.20
Average turn around time = 19.00	Average Response Time =2.80
Total no. of Context Switching Performed = 10
Running Round Robin scheduler...
pid       	arrival   	CPU-burst 	finish    	waiting time	turn around	response time	No. of Context
1         	0         	1         	2         	0         	2         	0         	1         
2         	2         	7         	62        	36        	60        	4         	4         
3         	3         	6         	42        	24        	39        	0         	2         
4         	5         	5         	74        	48        	69        	4         	4         
5         	6         	2         	15        	6         	9         	6         	0         
6         	10        	4         	48        	27        	38        	11        	1         
7         	12        	3         	50        	30        	38        	12        	1         
8         	13        	9         	89        	42        	76        	17        	5         
9         	17        	10        	87        	39        	70        	16        	4         
10        	18        	8         	81        	39        	63        	18        	3         
Average CPU Burst Time = 5.50	Average Waiting time = 29.10
Average turn around time = 46.40	Average Response Time =8.80
Total no. of Context Switching Performed = 25
Running Round Robin scheduler...
pid       	arrival   	CPU-burst 	finish    	waiting time	turn around	response time	No. of Context
1         	0         	10        	18        	2         	18        	0         	5         
2         	11        	8         	36        	3         	25        	1         	3         
3         	13        	2         	21        	3         	8         	5         	0         
4         	20        	7         	50        	5         	30        	4         	3         
5         	31        	12        	120       	56        	89        	5         	5         
6         	37        	9         	125       	57        	88        	5         	5         
7         	48        	9         	131       	60        	83        	3         	4         
8         	55        	6         	108       	39        	53        	8         	2         
9         	58        	3         	92        	24        	34        	11        	1         
10        	65        	8         	174       	82        	109       	13        	4         
11        	68        	5         	143       	62        	75        	16        	2         
12        	75        	5         	170       	77        	95        	45        	3         
13        	78        	2         	99        	18        	21        	18        	0         
14        	85        	7         	179       	74        	94        	23        	3         
15        	88        	4         	189       	79        	101       	26        	4         
16        	98        	1         	152       	45        	54        	52        	1         
17        	101       	6         	195       	61        	94        	25        	4         
18        	109       	6         	177       	51        	68        	23        	2         
19        	112       	3         	164       	42        	52        	26        	1         
20        	119       	8         	192       	48        	73        	25        	3         
Average CPU Burst Time = 6.05	Average Waiting time = 44.40
Average turn around time = 63.20	Average Response Time =16.70
Total no. of Context Switching Performed = 55
