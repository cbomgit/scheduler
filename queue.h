#include"process.h"


/*This header file defines functions that implement queuing operations. The
  queue will hold a process struct defining a process. This definition is in 
  process.h. A queue is defined as an array with max size MAX_QUEUE_LENGTH. No
  resizing operation is supported. The functions below require that the caller
  maintain a pointer to the front and back of the queue as well as the queue's
  current size.
  
*/

#define MAX_QUEUE_LENGTH 100

/*Adds an item to the end of the queue. If we have reached max capacity, then
  the program is abored.
*/

void queueProcess(process *readyQueue, int *tail, process newProc, int *size);

/*Removes and retrieves the item at the front of the queue. If the queue is
  empty, then a process with pid -1 is returned. Caller should check against
  the returned process pid.
*/
process dequeueProcess(process *readyQueue, int *head, int *size);

/*Internal function that adjusts the pointer to the head or tail of the queue.
  The pointer is implemented as an array index. If ndx reaches MAX_QUEUE_LENGTH
  it is set to 0.
*/
int increment(int ndx);
