/*Priority queue implementation. This header files defines functions that
  implements priority queue operations. The priority queue guarantees that
  the item at the front of the queue is the logically smallest value in the 
  collection. The priority queue only allows the removal of the smallest item
  and has a maximum capacity of MAX_QUEUE_LENGTH
  */
  
#include"process.h"


int addToShortestJobQueue(process p, int *currentSize, process *queue);

process viewShortestJob(int currentSize, process *queue);

process getShortestJob(int *currentSize, process *queue);



/*Non public interfaces */
void buildQueue(process *queue, int *currentSize);

void percolateDown(int currentSize, process *queue, int hole);



