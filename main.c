#include"queue.h"
#include"priorityQueue.h"
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>


void processArgs(int numArgs, char *argv[]);
int readFromFile(char *fileName, process *input);
void simulateRR(process *input, int inputSize, int timeSlice);
void simulateFCFS(process *input, int inputSize);
void simulateSRTF(process *input, int inputSize);
void printUsage();
void printStatistics(process *table, int size);

int main(int argc, char *argv[]) {

   int inputSize = 0;
   process input[MAX_QUEUE_LENGTH];


   processArgs(argc, argv);
   inputSize = readFromFile(argv[1], input);

   if(atoi(argv[2]) == 0) {
      simulateFCFS(input, inputSize);
   } else if(atoi(argv[2]) == 1) {
      simulateSRTF(input, inputSize);
   } else if(atoi(argv[2]) == 2) {
      simulateRR(input, inputSize, atoi(argv[3]));
   } else {
      printUsage();
   }
   
   return 0;
}
   

void printUsage() {

   printf("Usage: scheduler [Path to input file] [scheduler type] (scheduler options).\n");
   printf("Scheduler types: 0 - First Come First Serve\n1 - Shortest Remaining Time First\n");
   printf("2 - Round Robin (follow with timeslice length).\n");
   exit(EXIT_FAILURE);
}

void printStatistics(process *table, int size) {

   int i = 0;
   int totalCPUBurst = 0, totalWaitTime = 0, totalTurnAround = 0;
   int totalResponseTime = 0, totalContextSwitches = 0;

   printf("%-10s\t%-10s\t%-10s\t%-10s\t%-10s\t%-10s\t%-10s\t%-10s\n", "pid","arrival","CPU-burst", 
                                                                      "finish","waiting time", 
                                                                      "turn around","response time", 
                                                                      "No. of Context");
   for(i = 0; i < size; i++) {
      printf("%-10d\t%-10d\t%-10d\t%-10d\t%-10d\t%-10d\t%-10d\t%-10d\n", table[i].pid, table[i].arrivalTime,
                                                                         table[i].cpuBurst, table[i].finishTime,
                                                                         table[i].totalWaitTime,
                                                                         table[i].finishTime - table[i].arrivalTime,
                                                                         table[i].responseTime, table[i].numContextSwitches);


      totalCPUBurst += table[i].cpuBurst;
      totalWaitTime += table[i].totalWaitTime;
      totalTurnAround += table[i].finishTime - table[i].arrivalTime;
      totalResponseTime += table[i].responseTime;
      totalContextSwitches += table[i].numContextSwitches;
   }

   printf("Average CPU Burst Time = %.2f\tAverage Waiting time = %.2f\n",
           totalCPUBurst / (double)(size), totalWaitTime / (double)(size));

   printf("Average turn around time = %.2f\tAverage Response Time =%.2f\n",
           totalTurnAround / (double)(size), totalResponseTime / (double)(size));

   printf("Total no. of Context Switching Performed = %d\n", totalContextSwitches);

}

void simulateRR(process *input, int inputSize, int timeSlice) {

   process readyQueue[MAX_QUEUE_LENGTH];
   process finishedQueue[inputSize];
   process runningProc = input[0];
   int currentInput = 1, currentTime = 0;
   int front = 0, readySize = 0;
   int back = -1, i;

     
   while(currentInput < inputSize || readySize > 0 || runningProc.pid != -1) {

      if(input[currentInput].arrivalTime == currentTime) {
         queueProcess(readyQueue, &back, input[currentInput], &readySize);
         currentInput++;
      }

      if(currentTime % timeSlice == 0) {
          
         //running process has completed
         if(runningProc.timeLeft == 0) {
            runningProc.finishTime = currentTime;
            finishedQueue[runningProc.pid - 1] = runningProc;
         } else {
            //no existing process or running process is still active
            runningProc.numContextSwitches++;
            runningProc.lastPreemptTime = currentTime;
            queueProcess(readyQueue, &back, runningProc, &readySize);
         }
         //get the next process
         runningProc = dequeueProcess(readyQueue, &front, &readySize);
         //record response time if this is first time being active
         if(runningProc.cpuBurst == runningProc.timeLeft) {
            runningProc.responseTime = currentTime - runningProc.arrivalTime;
         }
      }else {

         //no process running, process running has completed, or
         //running process still has time left
         if(runningProc.pid == -1) {
            runningProc = dequeueProcess(readyQueue, &front, &readySize); 
            runningProc.responseTime = runningProc.responseTime == 0 ? currentTime : runningProc.responseTime;
         } else if(runningProc.timeLeft == 0) {
            runningProc.finishTime = currentTime;
            finishedQueue[runningProc.pid - 1] = runningProc;
            runningProc = dequeueProcess(readyQueue, &front, &readySize); 
            runningProc.responseTime = runningProc.responseTime == 0 ? currentTime : runningProc.responseTime;
         } else {
            runningProc.timeLeft--;
         }
      }
      currentTime++;

      for(i = front; i < back; i++) {
         if(i == MAX_QUEUE_LENGTH) 
            i = 0;
         readyQueue[i].totalWaitTime++;
      }
   }

   printStatistics(finishedQueue, inputSize);
}

void simulateFCFS(process *input, int inputSize) {

   process readyQueue[MAX_QUEUE_LENGTH];
   process finishedQueue[inputSize];
   process runningProc = input[0];
   int currentInput = 1, currentTime = 0;
   int front = 0, readySize = 0;
   int back = -1;
   int i;

   while(currentInput < inputSize || readySize > 0 || runningProc.pid != -1) {
   
      if(input[currentInput].arrivalTime == currentTime) {
         queueProcess(readyQueue, &back, input[currentInput], &readySize);
         currentInput++;
      } else {
         if(runningProc.pid == -1) {
            //no process is running so grab the next queued process
            runningProc = dequeueProcess(readyQueue, &front, &readySize);
            //record the response time if it hasn't been set yet
            if(runningProc.cpuBurst == runningProc.timeLeft) {
               runningProc.responseTime = currentTime - runningProc.arrivalTime;
            }
         } else if(runningProc.timeLeft == 0) {
            //current process has finished running
            runningProc.finishTime = currentTime;
            //store the finished processes in order by PID
            finishedQueue[runningProc.pid - 1] = runningProc;
            //grab the next process
            runningProc = dequeueProcess(readyQueue, &front, &readySize);
            //record the response time if it hasn't been set yet
            if(runningProc.cpuBurst == runningProc.timeLeft) {
               runningProc.responseTime = currentTime  - runningProc.arrivalTime;
            }
         } else {
            runningProc.timeLeft--;
         }
      }
      currentTime++;
      //increment each queued process' wait time
      for(i = front; i < back; i++) {
         if(i == MAX_QUEUE_LENGTH) {
            i = 0;
         }
         readyQueue[i].totalWaitTime++;
      }

   }

   printStatistics(finishedQueue, inputSize);

}

void simulateSRTF(process *input, int inputSize) {

   process finishedQueue[inputSize];
   process readyQueue[MAX_QUEUE_LENGTH];
   int currentTime = 0, queueSize = 0;
   int currentInput = 1;
   int i = 0;
   process runningProc = input[0];    

   while(queueSize > 0 || currentInput < inputSize || runningProc.pid != -1) {

      if(input[currentInput].arrivalTime == currentTime) {
      
         //check if we should preempt running process, only preempt if there is an existing
         //process to preempt
         if(runningProc.pid == -1 || runningProc.timeLeft > input[currentInput].cpuBurst) {
            runningProc.lastPreemptTime = currentTime;
            runningProc.numContextSwitches++;
            addToShortestJobQueue(runningProc, &queueSize, readyQueue);
            runningProc = input[currentInput];
            runningProc.responseTime = 0;
         } else {
            //process may have finished executing in this case
            if(runningProc.timeLeft == 0) {
               runningProc.finishTime = currentTime;
               finishedQueue[runningProc.pid - 1] = runningProc;
               runningProc = getShortestJob(&queueSize, readyQueue);
               if(runningProc.cpuBurst == runningProc.timeLeft) {
                  runningProc.responseTime = currentTime - runningProc.arrivalTime;
               }
            }
            //queue the new process
            addToShortestJobQueue(input[currentInput], &queueSize, readyQueue);
         }
         currentInput++;
      } else {
         //cases : no running processes, running process finishes execution,
         //running process does work
         if(runningProc.pid == -1) {
            runningProc = getShortestJob(&queueSize, readyQueue);
            if(runningProc.cpuBurst == runningProc.timeLeft) {
               runningProc.responseTime = currentTime - runningProc.arrivalTime;
            }
         } else if(runningProc.timeLeft == 0) {
            runningProc.finishTime = currentTime;
            finishedQueue[runningProc.pid - 1] = runningProc;
            runningProc = getShortestJob(&queueSize, readyQueue);
            if(runningProc.cpuBurst == runningProc.timeLeft) {
               runningProc.responseTime = currentTime - runningProc.arrivalTime;
            }
         } else {
            runningProc.timeLeft--;
         }
      } 
      currentTime++;

      for( ; i < queueSize; i++) {
         readyQueue[i].totalWaitTime++;
      }
   }

   printStatistics(finishedQueue, inputSize);
}

void processArgs(int numArgs, char *argv[]) {
   
   if(numArgs < 3) {
      printf("Not enough arguments.\n");
      printUsage();
   } else if((atoi(argv[2]) == 2 && numArgs != 4) || (atoi(argv[2]) < 2 && numArgs > 3)) {
      printf("Provide argument for scheduler to run, or if trying to run Round Robin, provide time slice.\n");
      printUsage();
   } else if(atoi(argv[2]) < 0 || atoi(argv[2]) > 2) {
      printf("%s is not a valid scheduler type.\n", argv[2]);
      printUsage();
   }
}

int readFromFile(char *fileName, process *input) {

   int inputSize = 0;
   int tmpPID, tmpArrival, tmpCPUBurst;
   FILE *fp = fopen(fileName, "r");    
   if(fp == NULL) {

      printf("Could not open file %s. Exiting.\n", fileName);
      exit(EXIT_FAILURE);
   }

   while(fscanf(fp, "%d %d %d",&tmpPID, &tmpArrival, &tmpCPUBurst) != EOF) {

      input[inputSize].pid = tmpPID;
      input[inputSize].arrivalTime = tmpArrival;
      input[inputSize].cpuBurst = tmpCPUBurst;
      input[inputSize].lastSwitchTime = 0;
      input[inputSize].finishTime = 0;
      input[inputSize].totalWaitTime = 0;
      input[inputSize].responseTime = 0;
      input[inputSize].numContextSwitches = 0;
      input[inputSize].timeLeft = tmpCPUBurst;
      input[inputSize].lastPreemptTime = 0;
      inputSize++;
   }

   return inputSize;
}
